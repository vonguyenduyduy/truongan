<div class="footer pt_60">
      <div class="container">
        <div class="row">
          <div class="col-md-3 footer-block">
            <div class="content_footercms_right">
              <div class="footer-contact">
                <div class="footer-logo mb_40">
                  <a href="index.html">
                    <img  src="<?php bloginfo('template_directory') ?>/images/logo.png" alt="logo">
                  </a>
                </div>
                <ul>
                  <li>CN1: 511 - 513 An Dương Vương - Phường 9 - Quận 5 - Tp. HCM</li>
                  <li>CN2: 538 An Dương Vương - Phường 9 - Quận 5 - Tp. HCM</li>
                  <li>Email: ototruongan@gmail.com.</li>
                </ul>
                <div class="social_icon">
                  <ul>
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-google"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-2 footer-block">
            <h6 class="footer-title ptb_20">Tuyển dụng</h6>
            <ul>
              <li><a href="#">Tin tuyển dụng</a></li>
            </ul>
          </div>
          <div class="col-md-3 footer-block">
            <h6 class="footer-title ptb_20">Thông tin thanh toán</h6>
            <ul>
              <li><a href="#">Vietcombank</a></li>
              <li><a href="#">Saccombank</a></li>
            </ul>
          </div>
          <div class="col-md-3 footer-block">
            <h6 class="footer-title ptb_20">Liên hệ</h6>
            <ul>
              <li><a href="#">Liên hệ với chúng tôi</a></li>
              <li><a href="#">Về chúng tôi</a></li>
            </ul>
          </div>

        </div>
      </div>
      <div class="footer-bottom mt_60 ptb_10">
        <div class="container">
          <div class="row">
            <div class="col-sm-6">
              <div class="copyright-part">@ 2019 Nội thất Ô tô Trường An</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php wp_footer(); ?>
    <!-- =====  FOOTER END  ===== -->
  </div>
  <a id="scrollup">Scroll</a>
  <script src="<?php bloginfo('template_directory') ?>/js/jQuery_v3.1.1.min.js"></script>
  <script src="<?php bloginfo('template_directory') ?>/js/owl.carousel.min.js"></script>
  <script src="<?php bloginfo('template_directory') ?>/js/bootstrap.min.js"></script>
  <script src="<?php bloginfo('template_directory') ?>/js/jquery.magnific-popup.js"></script>
  <script src="<?php bloginfo('template_directory') ?>/js/jquery.firstVisitPopup.js"></script>
  <script src="<?php bloginfo('template_directory') ?>/js/custom.js"></script>
</body>

</html>
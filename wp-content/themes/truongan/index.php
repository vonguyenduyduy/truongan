<?php get_header();?>
<!-- =====  CONTAINER START  ===== -->
<div class="container">
  <div class="row ">
    <?php get_sidebar();?>
    <div id="column-right" class="col-sm-8 col-md-8 col-lg-9 mtb_30">
      <!-- =====  BANNER STRAT  ===== -->
      <div class="banner">
        <div class="main-banner owl-carousel">
          <div class="item"><a href="#">
            <img src="https://znews-photo.zadn.vn/w860/Uploaded/rik_rdcvcvwt_wc/2016_04_15/a1_Audi_TTS.jpg"
                alt="Main Banner" class="img-responsive" /></a></div>
          <div class="item"><a href="#">
            <img src="https://znews-photo.zadn.vn/w860/Uploaded/rik_rdcvcvwt_wc/2016_04_15/a1_Audi_TTS.jpg"
                alt="Main Banner" class="img-responsive" /></a></div>
        </div>
      </div>


      <!-- =====  PRODUCT TAB  ===== -->
      <?php get_template_part('template-parts/home-products-new') ?>
      <!-- =====  BANNER START  ===== -->
		  <?php get_template_part('template-common/banner-1') ?>
      <!-- =====  BANNER END  ===== -->
      <?php get_template_part('template-parts/home-products-hot') ?>
		<?php get_template_part('template-common/banner-2') ?>
      <?php get_template_part('template-parts/home-products-promotion') ?>
		<?php get_template_part('template-common/banner-1') ?>

      <!-- =====  PRODUCT TAB  END ===== -->
      <!-- =====  Blog ===== -->
      <div id="Blog" class="mt_40">
        <div class="heading-part mb_20 ">
          <h2 class="main_title">Tin mới nhất</h2>
        </div>
        <?php get_template_part('template-parts/new-index') ?>
        <!-- =====  Blog end ===== -->
      </div>
    </div>
  </div>
<!--  --><?php //get_template_part('template-parts/brand') ?>
</div>
</div>
<!-- =====  CONTAINER END  ===== -->
<!-- =====  FOOTER START  ===== -->
<?php get_footer(); ?>
<div class="blog-contain box">
    <div class="blog owl-carousel ">
        <?php $postquery = new WP_Query(array('posts_per_page' => 6));
						if ($postquery->have_posts()) {
						while ($postquery->have_posts()) : $postquery->the_post();
						$do_not_duplicate = $post->ID;
						?>

        <div class="item">
            <div class="box-holder">
                <div class="thumb post-img">
                    <a href="<?php the_permalink(); ?>">
                        <img class="news-home-img" src=" <?php the_post_thumbnail_url(); ?>" alt="Tin tuc">
                </div>
                <div class="post-info mtb_20 ">
                    <h6 class="mb_10 text-uppercase"> <a href="<?php the_permalink(); ?>"> <?php the_title();  ?></a>
                    </h6>
                    <p><?php the_excerpt(); ?></p>
                    <div class="date-time">
                        <div class="day"> <?php echo get_the_date('d'); ?></div>
                        <div class="month"><?php echo get_the_date('m'); ?></div>
                    </div>
                </div>
            </div>
        </div>



<?php endwhile;} ?>
</div>
</div>
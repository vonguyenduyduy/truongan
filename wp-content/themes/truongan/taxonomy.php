<?php get_header();?>
<!-- =====  CONTAINER START  ===== -->

<div class="container">
  <div class="row ">
    <?php get_sidebar();?>
    <div class="col-sm-8 col-md-8 col-lg-9 mtb_30">
      <div class="breadcrumb ptb_20">
        <h1>Products</h1>
        <ul>
          <li><a href="index.html">Home</a></li>
          <li class="active">Products</li>
        </ul>
      </div>

      <div class="row">
        <?php 
          $query = array(
            'post_type' => 'san_pham',
            'tax_query' => array(
             array(
              'taxonomy' => 'hang_xe',
              'field' => 'slug',
              'terms' => 'kia'
             )
            )
           );
           $arr_posts = new WP_Query( $query );
           if ( $arr_posts->have_posts() ) :
            while ( $arr_posts->have_posts() ) : $arr_posts->the_post();?>

        <div style="height:350px" class="product-layout  product-grid  col-lg-3 col-md-4 col-sm-6 col-xs-12 ">
          <div class="item">
            <div class="product-thumb clearfix mb_30">
              <div class="image product-imageblock">
                <a href="<?php the_permalink() ;?>">
                  <img style="    height: 150px;width: 100%;" data-name="product_image"
                    src=" <?php the_post_thumbnail_url(array(500,200)); ?>" alt="iPod Classic"
                    title="iPod Classic" class="img-responsive">
                  <img style="height: 150px;width: 100%;" src=" <?php the_post_thumbnail_url(array(500,200)); ?>" alt="iPod Classic"
                    title="iPod Classic" class="img-responsive">
                </a>
              </div>
              <div class="caption product-detail text-left">
                <h6 data-name="product_name" class="product-name mt_20">
                  <a href="<?php the_permalink() ;?>"
                    title="Casual Shirt With Ruffle Hem"><?php the_title() ;?></a></h6>
                <div class="rating">
                  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i
                      class="fa fa-star fa-stack-1x"></i></span>
                  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i
                      class="fa fa-star fa-stack-1x"></i></span>
                  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i
                      class="fa fa-star fa-stack-1x"></i></span>
                  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i
                      class="fa fa-star fa-stack-1x"></i></span>
                  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i
                      class="fa fa-star fa-stack-x"></i></span>
                </div>
                <span class="price"><span class="amount">
                  <b style="color:red;font-size:15px">Liên hệ 0918 029 627 (Mr.Bắc)</b>
                  <!-- <span class="currencySymbol"> Liên hệ </span>
                  </span> -->
                  <p class="product-desc mt_20 mb_60"></p>
                  <!-- <div class="button-group text-center">
                      <div class="wishlist"><a href="#"><span>wishlist</span></a></div>
                      <div class="quickview"><a href="#"><span>Quick View</span></a></div>
                      <div class="compare"><a href="#"><span>Compare</span></a></div>
                      <div class="add-to-cart"><a href="#"><span>Add to cart</span></a></div>
                    </div> -->
              </div>
            </div>
          </div>
        </div>
        <?php
            endwhile;
            endif;
            wp_reset_query();
         ?>


      </div>
      <div class="pagination-nav text-center mt_50">
        <ul>
          <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
          <li class="active"><a href="#">1</a></li>
          <li><a href="#">2</a></li>
          <li><a href="#">3</a></li>
          <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
        </ul>
      </div>
      <?php get_template_part('template-parts/brand') ?>

    </div>
  </div>
</div>
<!-- =====  CONTAINER END  ===== -->
<!-- =====  FOOTER START  ===== -->
<?php get_footer(); ?>
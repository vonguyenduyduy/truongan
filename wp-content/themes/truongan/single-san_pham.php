
<?php get_header();?>
    <!-- =====  CONTAINER START  ===== -->
    <div class="container">
      <div class="row ">
         <?php get_sidebar();?>
       
        <div class="col-sm-8 col-md-8 col-lg-9 mtb_10">
          <!-- =====  BANNER STRAT  ===== -->
          <div class="breadcrumb ptb_20">
            <h1>SẢN PHẨM</h1>
          </div>
          <!-- =====  BREADCRUMB END===== -->
          <?php if (have_posts()) : ?>
              <?php while (have_posts()) : the_post(); ?>
              <div class="row mt_10 ">
            <div class="col-md-6">
              <div>
                <a class="thumbnails"> 
                  <img data-name="product_image" src="<?php the_post_thumbnail_url(array(500,200)); ?>" alt="" />
                </a>
                </div>
            </div>
            <div class="col-md-6 prodetail caption product-thumb">
              <h4 data-name="product_name" class="product-name">
                <a href="#" title="Casual Shirt With Ruffle Hem"><?php the_title(); ?></a>
              </h4>
              <div class="rating">
                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
              </div>
              <span class="price mb_20"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
              </span>
              <hr>
              <ul class="list-unstyled product_info mtb_20">
                <li>
                  <label>Mã sản phẩm:</label>
                  <span> XXXXXX </span>
                </li>
                <li>
                  <label>Thương hiệu:</label>
                  <span> <a href="#">Apple</a></span></li>
                <li>
                  <label>Nhóm sản phẩm:</label>
                  <span> XXXXXX </span>
                </li>
                <li>
                  <label>Lượt xem:</label>
                  <span> XXXXXX </span>
                </li>
              </ul>
              <hr>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div id="exTab5" class="mtb_30">
                <ul class="nav nav-tabs">
                  <li class="active"> <a href="#1c" data-toggle="tab">Chi tiết sản phẩm</a> </li>
                </ul>
                <div class="tab-content mt_20">
                  <div class="tab-pane active" id="1c">
                    <p>
							       <?php the_excerpt(); ?>
                    </p>
                  </div>

                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="heading-part text-center">
                <h2 class="main_title mt_50">Sản phẩm khác</h2>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="product-layout  product-grid related-pro  owl-carousel mb_50">
            <?php 
            $query = array(
              'post_type' => 'san_pham',
              'tax_query' => array(
              array(
                'taxonomy' => 'hang_xe',
                'field' => 'slug',
                'terms' => 'kia'
              )
              )
            );
            $arr_posts = new WP_Query( $query );
            if ( $arr_posts->have_posts() ) :
              while ( $arr_posts->have_posts() ) : $arr_posts->the_post();?>
            <div class="item">
                  <div class="product-thumb">
                    <div class="image product-imageblock"> 
                      <a href="<?php the_permalink() ;?>">
                      <img data-name="product_image" src=" <?php the_post_thumbnail_url(array(500,200)); ?>" alt="iPod Classic" title="iPod Classic" class="img-responsive product-h200"> 
                      <img src=" <?php the_post_thumbnail_url(array(500,200)); ?>" alt="iPod Classic" title="iPod Classic" class="img-responsive product-h200">
                      </a>
                      </div>
                    <div class="caption product-detail text-left">
                      <h6 data-name="product_name" class="product-name mt_20">
                        <a href="<?php the_permalink() ;?>" title="Casual Shirt With Ruffle Hem">
                        <?php the_title() ;?>
                        </a>
                      </h6>
                      <div class="rating">
                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                      </div>
                      <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                      </span>
                    </div>
                  </div>
                </div>
              <?php
                  endwhile;
                  endif;
                  wp_reset_query();
              ?>
            
              </div>
            </div>
          <?php endwhile;?>
                <?php endif; ?>
                <?php get_template_part('template-parts/brand') ?>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>

    <!-- =====  CONTAINER END  ===== -->
    <!-- =====  FOOTER START  ===== -->
    <?php get_footer(); ?>


<?php

if(!function_exists('theme_setup')){
    function theme_setup(){
        register_nav_menu( 'primary', __( 'Primary Menu'));
    }

    add_action('init','theme_setup');
		add_theme_support( 'post-thumbnails' );

}


add_filter( 'nav_menu_css_class', 'menu_item_classes', 10, 4 );

function menu_item_classes( $classes, $item, $args, $depth ) {

    unset($classes);

    $classes[] = 'dropdown';

    return $classes;
}





?>



